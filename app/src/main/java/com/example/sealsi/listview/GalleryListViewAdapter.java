package com.example.sealsi.listview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class GalleryListViewAdapter extends ArrayAdapter<GalleryImage> {

    private final Context mContext;
    private final ArrayList<GalleryImage> mGalleryImageList;

    public GalleryListViewAdapter(Context context, ArrayList<GalleryImage> objects) {
        super(context, R.layout.galleryrowlayout, objects);

        mContext = context;
        mGalleryImageList = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.galleryrowlayout, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.label);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);

        textView.setText(mGalleryImageList.get(position).text);
        imageView.setImageResource(mGalleryImageList.get(position).id);

        return rowView;
    }

}
